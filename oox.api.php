<?php

/**
 * @file
 * Oox module hooks definition and documentation.
 */

/**
 * Allow modules to interact with external libraries oox.
 * 
 * @param string $op
 *   Can be:
 *    - 'path' : If its being the first time the library has been asked by
 *      another module, before doing any path lookup in well known path
 *      candidates, this will let a change for other module to register
 *      their own path for the asked library.
 *      The returned path can be either a Drupal root relative path, or the
 *      FS absolute path.
 *      If more than one module answer a path here, each one of them will be
 *      tested in the module's weight order, the first working path of those
 *      will be used.
 *   - 'info' : Any module can return a descriptive string about the given
 *     library. Each message from each module will be displayed in the
 *     administration page.
 * @param string $library
 *   Library canonical name.
 * 
 * @return mixed
 *   When op is:
 *     - 'path' : Must return a valid path string for the given library, this
 *       path must be an existing folder.
 *     - 'info' : Must return a localized descriptive message about the current
 *       given library.
 */
function hook_oox_library_api($op, $library, $edit = array()) {
  
}

/**
 * Define a custom registry. Whatever happens here, if you only need the default
 * OoxRegistry implementation, you are allowed not to define this hook.
 * 
 * Be gentle with factories, their definition are not cached (yet), use the
 * default implementation when you can.
 * 
 * In the registry definition, all keys are optional. You can even not define
 * your registry, in which case it will be spawned using the default
 * implementation when asked.
 * 
 * Notice that if you don't define it in your module, you won't be able to
 * access its options in the "registries" administration screen. All usage will
 * be dynamic, at run time.
 * 
 * Options are:
 *   - 'class' : Class name that represent the registry. For some specific
 *     purpose, you can override the OoxRegistry class. Not that if you
 *     specify a class here, the OoxRegistry override will be checked.
 *     If the base implementation suits you, do not specify the class name.
 *   - 'file' : If you overrided the OoxRegistry with your own
 *     implementation, you can use this option to specify the PHP file path
 *     where is stored your class definition.
 *   - 'hook' : If defined, the hook used in order to define specific objet
 *     classes will be the given value. If you don't specify it, it will be the
 *     default hook_oox_registry_TYPE(), where TYPE is the items array
 *     key. Wheither or not you override the hook name, the hook signature will
 *     remains the same.
 * 
 * @see hook_oox_registry_TYPE()
 */
function hook_oox_registry() {
  $items[] = array();
  // Key is the registry type.
  $items['mymodule_registry'] = array(
    'class' => 'MyModuleRegistry',
    'file' => drupal_get_path('module', 'mymodule') . '/lib/MyModuleRegistry.php',
    'hook' => 'mymodule_items',
  );
  return $items;
}

/**
 * Define available items for the given registry.
 * 
 * TYPE refers to the given factory type.
 * 
 */
function hook_oox_registry_TYPE() {
  $items[] = array();
  // There is no naming convention at all, just be aware of potential conflicts
  // between modules.
  $items['mymodule_TYPE_item'] = array(
    // Same note here, no forced naming convention.
    'class' => 'MyModuleTYPEItem',
    // Name and file are optional, name is highly recommended while file is
    // only a simple helper for you if you don't want to do the require_once
    // yourself.
    // If you set a file path, and the registry does not find it at build time,
    // it will check if the class exists, if so, it won't fail.
    // Do not localize the name, else it will be cached with a localized name.
    'name' => "Some name",
    'file' => drupal_get_path('module', 'mymodule') . '/lib/MyModuleTYPERegistry.php',
  );
  return $items;
}
