<?php

/**
 * This class intends to provide a default IOptions storage mecanism. Notice
 * that all these options will be stored in the same database table, whatever
 * their registry type or type is.
 * 
 * If you use your own factory for business objects that implements this
 * interface, you can bypass this factory and use your own.
 * 
 * For performance reasons, and because we have a totally different schema than
 * the xoxo object table, we are going to use another custom factory
 * mecanism.
 * 
 * This factory still does not provide caching, promise, it will soon.
 */
class OptionsFactory
{
  /**
   * @var OptionsFactory
   */
  public static $__instance;

  /**
   * Get singleton.
   * 
   * @return OptionsFactory
   */
  public static function getInstance() {
    if (!isset(self::$__instance)) {
      self::$__instance = new OptionsFactory();
    }
    return self::$__instance;
  }

  /**
   * Singleton implementation.
   */
  private function __construct() { }

  /**
   * Object cache.
   * 
   * @var array.
   */
  protected $_cache = array();

  /**
   * Load an IOptionable instance. This method will be statically cached, and
   * instances are being shared (no clone).
   * 
   * @param int $oid
   *   Options instance unique identifier.
   * @param IOptionnable $object = NULL
   *   (optional) If you are trying to load options for an already loaded
   *   instance, pass it here. Notice that no class or registry type checks
   *   will be done.
   * 
   * @return IOptionable
   *   Fully loaded object. If $object parameter is set, return the exact same
   *   instance.
   * 
   * @throws OoxException
   *   If section does not exists or registry has not been found.
   */
  public function load($oid, IOptionable $object = NULL) {
    $hasObject = $object instanceof IOptionable;

    if (!array_key_exists($oid, $this->_cache) || $hasObject) {
      $type = NULL;
      $options = array();

      if (!$data = db_fetch_object(db_query("SELECT * FROM {oox_options} WHERE oid = %d", $oid))) {
        throw new OoxException("Metadata for options " . $oid . " does not exists");
      }

      if (!$hasObject) {
        // Check registry exists.
        if (!($registry = OoxRegistry::getInstance($data->registry_type))) {
          throw new OoxException("Unable to load registry type " . $data->registry_type . " when loading options " . $oid);
        }
  
        // Check item can be instanciated.
        if (!($object = $registry->getItem($data->type))) {
          throw new OoxException("Unable to instanciate object type " . $data->type . " when loading options " . $oid);
        }

        $object->_setType($data->type);
        $object->_setRegistryType($data->registry_type);
      }

      // Build the instance.
      $result = db_query("SELECT * FROM {oox_options_data} WHERE oid = %d", $oid);
      while ($row = db_fetch_object($result)) {
        $options[$row->name] = unserialize($row->value);
      }
      $object->setOptions($options);
      $object->_setOptionsIdentifier($oid);

      if (!$hasObject) {
        $this->_cache[$oid] = $object;
      }
      else {
        return $object;
      }
    }

    return $this->_cache[$oid];
  }

  /**
   * Insert or update an instance. If this instance does not exists in
   * database, its own identifier will be set.
   * 
   * @param ISection $section
   *   Section to save.
   * 
   * @throws OoxException
   *   In case of any error.
   */
  public function save(IOptionable $options) {
    // For performance reasons, we wrap all this in a transaction, which will
    // force the table bulk update and indexing.
    db_query("BEGIN");

    // For performances reasons, we are going to make a BULK insert instead of
    // relying on the drupal_write_record() function.
    if ($oid = $options->getOptionsIdentifier()) {
      db_query("DELETE FROM {oox_options_data} WHERE oid = %d", $oid);
      // Also clear cache.
      unset($this->_cache[$oid]);
    }
    else {
      $type = $options->getType();
      $registryType = $options->getRegistryType();
      // Insert first row, in order to get back new identifier.
      // We don't store the type as a serialized value.
      db_query("INSERT INTO {oox_options} (type, registry_type) VALUES ('%s', '%s')", array($type, $registryType));
      $oid = db_last_insert_id('section_option', 'oid');
      $options->_setOptionsIdentifier($oid);
    }

    // Prepare bulk insert.
    $values = array();
    $args = array();
    foreach ($options->getOptions() as $name => $value) {
      $values[] = "(%d, '%s', %b)";
      $args[] = $oid;
      $args[] = $name;
      $args[] = serialize($value);
    }
    // Real bulk insert.
    if (!empty($values)) {
      db_query("INSERT INTO {oox_options_data} (oid, name, value) VALUES " . implode(", ", $values), $args);
    }

    // End the transaction, and do some error handling.
    db_query("COMMIT");
    if (db_error()) {
      db_query("ROLLBACK");
      throw new SectionException("Error during save (or update)");
    }
  }
}
