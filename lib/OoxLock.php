<?php

/**
 * Exception thrown when aptempting to acquire an already locked lock.
 */
class LockAlreadyAcquiredException extends OoxException
{
  /**
   * Lock database row.
   * 
   * @var object
   */
  protected $_lock;

  /**
   * Current user static cache, since Drupal does not do cache into the
   * user_load() function.
   */
  protected $_user;

  /**
   * Get user which owns the current lock.
   * 
   * @return object
   *   Drupal database account row.
   */
  public function getAccount() {
    if (!isset($this->_user)) {
      if ($this->_lock->uid) {
        $this->_user = user_load($this->_lock->uid);
      }
      // Create a fake user, with a non existing uid, which name is
      // the current translation of "System".
      else {
        $this->_user = new stdClass();
        $this->_user->uid = -1;
        $this->_user->name = t("System");
        $this->_user->mail = variable_get('site_mail', '');
      }
    }
    return $this->_user;
  }

  /**
   * Get the lock database row.
   */
  public function getLock() {
    return $this->_lock;
  }

  /**
   * Build instance.
   * 
   * @param object $lock
   *   Lock database row.
   */
  public function __construct($lock) {
    parent::__construct("Already locked lock");
    $this->_lock = $lock;
  }
}

/**
 * Simple semaphore based persistant locking framework. This interface defines
 * the lock backend API.
 * 
 * Locks are acquired on a name basis. Each acquired lock will raise exception
 * on new acquire attempts until it's being either pragmaticaly released, or
 * when its expiration time comes out of date.
 * 
 * For each lock, you can also give a token value. Any locking attempt of the
 * exact same lock with the exact same token will return sucessful.
 * 
 * If user is currently logged, each semaphore can store its uid. When done,
 * all current active locks will be released on user session destroy.
 * 
 * A default backend using a database table for locking is given, and used per
 * default.
 * 
 * @see OoxLock
 * @see oox_user()
 */
interface ILock
{
  const DEFAULT_GROUP = 'default';

  /**
   * Acquire a lock.
   * 
   * @param string $name
   *   Lock name.
   * @param int $lifetime = LIFETIME_DEFAULT
   *   (optional) Lock lifetime (in seconds).
   * @param string $group = ILock::DEFAULT_GROUP
   *   (optional) A group name for lock, this will allow to remove all locks for
   *   a certain group.
   * @param boolean $forUser = FALSE
   *   (optional) If set to TRUE, the current lock will be linked to the current
   *   logged in user (except for anonymous). This means that on session destroy
   *   if no other session is active, the lock will be removed.
   * @param string $token = NULL
   *   (optional) If set, set a token the lock. Tokens will allow more than one
   *   locking attempt on the same lock, using the same token. It can also allow
   *   lock sharing among users.
   * 
   * @throws LockAlreadyAcquiredException
   *   If lock already been acquired by another thread.
   */
  public function acquire($name, $lifetime = LIFETIME_DEFAULT, $group = ILock::DEFAULT_GROUP, $forUser = FALSE, $token = NULL);

  /**
   * Release a lock.
   * If lock is already free or does not exists, remains silent.
   * 
   * @param string $name
   *   Lock name.
   */
  public function release($name);

  /**
   * Get alive lock.
   * 
   * @param string $name
   *   Lock name.
   * @param string $group = NULL
   *   (optional) If set, will attempt to find by matching group.
   * @param int $uid = NULL
   *   (optional) If set, will attempt to find by matching uid.
   * @param string $token
   *   (optional) If set, will attempt to find by matching token.
   * 
   * @return object
   *   Lock database object if locked, FALSE else.
   */
  public function getLock($name, $group = NULL, $uid = NULL, $token = NULL);

  /**
   * Clear outdated locks.
   * 
   * Both of the following parameters can be used at once.
   * 
   * @param int $uid = 0
   *   (optional) If set, this will only wipe out all locks for the given
   *   user identifier.
   * @param string $group = NULL
   *   (optional) If set, this will wipe out all locks for the given group.
   * @param string $token = NULL
   *   (optional) If set, this will wipe out all locks for the given token.
   */
  public function wipeOutLocks($uid = 0, $group = NULL, $token = NULL);

  /**
   * Some cache backend can not run on some environment. This function will be
   * called at singleton instanciation time, if it returns FALSE, the singleton
   * instanciation will drop to the default backend as failsafe choice.
   * 
   * Remember that this check will done at each page request, it is your job to
   * ensure it won't be ressource intensive.
   * 
   * @return boolean
   */
  public function checkEnvironment();
}

/**
 * Default implementation, and singleton implementation. Fine for most usages.
 */
class OoxLock implements ILock
{
  /**
   * Default arbitrary lifetime for lock. 
   * 
   * @var int
   */
  const LIFETIME_DEFAULT = 30;

  /**
   * Singleton implementation.
   * 
   * @var ILock
   */
  private static $__instance;

  /**
   * Singleton implementation, get instance.
   * 
   * @return ILock
   */
  public static function getInstance() {
    if (!isset(self::$__instance)) {
      // Allow runtime implementation change. Some users might want to use
      // a cache based implementation (using memcache for example could be
      // a lot faster than use database queries).
      if ($class = variable_get(OOX_VAR_LOCK_BACKEND, 0)) {
        if (!oox_class_is($class, 'ILock')) {
          $class = 'OoxLock';
          watchdog('oox', "Defined lock implementation " . $class . " does not implements the ILock interface", NULL, WATCHDOG_ERROR);
        }
        else {
          self::$__instance = new $class();
          if (!self::$__instance->checkEnvironment()) {
            unset(self::$__instance);
            watchdog('oox', "Your environement does not matches the " . $class . " implementation requirements", NULL, WATCHDOG_ERROR);
          }
        }
      }
      // Failsafe, if not instanciated upper, use the default implementation.
      if (!isset(self::$__instance)) {
        self::$__instance = new OoxLock();
      }
    }
    return self::$__instance;
  }

  /**
   * (non-PHPdoc)
   * @see ILock::acquire()
   */
  public function acquire($name, $lifetime = LIFETIME_DEFAULT, $group = ILock::DEFAULT_GROUP, $forUser = FALSE, $token = NULL) {
    $time = time();
    // Aptempt to find if this semaphore already been locked.
    if ($lock = db_fetch_object(db_query("SELECT * FROM {semaphore_oox} WHERE expire > %d AND name = '%s'", array($time, $name)))) {
      // Success in case of identical tokens.
      if ($token && $lock->token == $token) {
        return TRUE;
      }
      else {
        throw new LockAlreadyAcquiredException($lock);
      }
    }
    else {
      // Avoid collisions when an expired semaphore still exists with the
      // same name.
      db_query("DELETE FROM {semaphore_oox} WHERE name = '%s'", array($name));
    }
    // Get user information.
    if ($forUser) {
      global $user;
      $uid = $user->uid;
    }
    else {
      $uid = 0;
    }
    // If none found, then aptempt the acquisition transaction.
    db_query("START TRANSACTION");
    if ($token) {
      db_query("INSERT INTO {semaphore_oox} (name, expire, value, token, uid) VALUES ('%s', %d, '%s', '%s', %d)", array($name, $time + $lifetime, $group, $token, $uid));
    }
    else {
      db_query("INSERT INTO {semaphore_oox} (name, expire, value, uid) VALUES ('%s', %d, '%s', %d)", array($name, $time + $lifetime, $group, $uid));
    }
    db_query("COMMIT");
    // Check for transaction error.
    if (db_error()) {
      db_query("ROLLBACK");
      // Specific case, that should not happens that often, two transactions
      // have been overlapping each other. We should fetch back the current
      // lock and throw the exception we the right user.
      $lock = db_fetch_object(db_query("SELECT * FROM {semaphore_oox} WHERE expire > %d AND name = '%s'", array($time, $name)));
      throw new LockAlreadyAcquiredException($lock);
    }
  }

  /**
   * (non-PHPdoc)
   * @see ILock::release()
   */
  public function release($name) {
    db_query("START TRANSACTION");
    db_query("DELETE FROM {semaphore_oox} WHERE name = '%s'", array($name));
    db_query("COMMIT");
  }

  /**
   * (non-PHPdoc)
   * @see ILock::getLock()
   */
  public function getLock($name, $group = NULL, $uid = NULL, $token = NULL) {
    // Prepare query.
    $where = array("expire > %d", "name = '%s'");
    $args = array(time(), $name);
    // Check for given group
    if ($group) {
      $where[] = "value = '%s'";
      $args[] = $group;
    }
    if ($uid) {
      $where[] = "uid = %d";
      $args[] = $uid;
    }
    if ($token) {
      $where[] = "token = '%s'";
      $args[] = $token;
    }
    return db_fetch_object(db_query("SELECT * FROM {semaphore_oox} WHERE " . implode(" AND ", $where), $args));
  }

  /**
   * (non-PHPdoc)
   * @see ILock::wipeOutLocks()
   */
  public function wipeOutLocks($uid = 0, $group = NULL, $token = NULL) {
    // Prepare query.
    $where = array();
    $args = array();
    // Check for given group
    if ($group) {
      $where[] = "value = '%s'";
      $args[] = $group;
    }
    if ($uid) {
      $where[] = "uid = %d";
      $args[] = $uid;
    }
    if ($token) {
      $where[] = "token = '%s'";
      $args[] = $token;
    }
    if (!empty($where)) {
      // If we have specific data, release all given locks.
      db_query("START TRANSACTION");
      db_query("DELETE FROM {semaphore_oox} WHERE " . implode(" AND ", $where), $args);
      db_query("COMMIT");
    }
    else {
      // Release all outdated locks.
      db_query("DELETE FROM {semaphore_oox} WHERE expire < '%d'", array(time()));
    }
  }

  /**
   * (non-PHPdoc)
   * @see ILock::checkEnvironment()
   */
  public function checkEnvironment() {
    return TRUE;
  }
}
