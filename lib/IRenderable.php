<?php

/**
 * This interface represents any kind of objects that can be rendered to end
 * user on site.
 */
interface IRenderable
{
  /**
   * Render the object preview. Commonly used for administration pages, it
   * will also be exposed to views module.
   * 
   * @return string
   *   (x)html output.
   */
  public function renderPreview();

  /**
   * Render the full, final output.
   * 
   * @return string
   *   (x)html output.
   */
  public function render();
}
