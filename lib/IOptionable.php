<?php

/**
 * Define an objet that can have a set of options.
 * 
 * The IRegistrable interface tells us the IOptions instances are object that
 * can be loaded using a specific registry.
 */
interface IOptionable extends IRegistrable
{
  /**
   * Get options storage identifier.
   * 
   * @return int
   *   Can return NULL if object is not stored.
   */
  public function getOptionsIdentifier();

  /**
   * Set options storage identifier. This is an internal method for the
   * factory only, please do not use it.
   * 
   * @param int $oid
   *   Options storage identifier.
   */
  public function _setOptionsIdentifier($oid);

  /**
   * Get options count.
   * 
   * return int
   */
  public function getOptionsCount();

  /**
   * Set all instance specific options. This likely will be used only by
   * factories and registries, and such, and should never be called during
   * the objet normal life line.
   * 
   * @param array $options
   *   Options.
   */
  public function setOptions($options);

  /**
   * Get specific options for an instance. 
   * 
   * @return array
   *   Options.
   */
  public function getOptions();

  /**
   * Shortcut that set a specific option.
   * 
   * @param string $name
   *   Option name.
   * @param mixed $value
   *   Option value.
   */
  public function setOption($name, $value);

  /**
   * Shortcut that get a specific option.
   * 
   * @param string $name
   *   Option name.
   * @param mixed $default = NULL
   *   (optional) Default value to return if option is not found.
   * 
   * @return mixed
   *   Option value, NULL if not found (or if the value is NULL).
   */
  public function getOption($name, $default = NULL);

  /**
   * Tells if option is set.
   * 
   * @return boolean
   *   TRUE if option is set (even if empty), FALSE else.
   */
  public function hasOption($name);
}

/**
 * Default implementation that can be used as base for buildind specific
 * implementation, either by inheritance, either by composition.
 */
class Options extends Registrable implements IOptionable
{
  /**
   * @var int
   */
  protected $_optionsIdentifier = NULL;

  /**
   * (non-PHPdoc)
   * @see IOptionable::getOptionsIdentifier()
   */
  public function getOptionsIdentifier() {
    return $this->_optionsIdentifier;
  }
  
  /**
   * (non-PHPdoc)
   * @see IOptionable::_setOptionsIdentifier()
   */
  public function _setOptionsIdentifier($oid) {
    $this->_optionsIdentifier = $oid;
  }

  /**
   * @var array
   */
  protected $_options = array();

  /**
   * (non-PHPdoc)
   * @see IOptionable::getOptionsCount()
   */
  public function getOptionsCount() {
    return count($this->_options);
  }

  /**
   * (non-PHPdoc)
   * @see IOptionable::setOptions()
   */
  public function setOptions($options) {
    $this->_options = $options;
  }

  /**
   * (non-PHPdoc)
   * @see IOptionable::getOptions()
   */
  public function getOptions() {
    return $this->_options;
  }

  /**
   * (non-PHPdoc)
   * @see IOptionable::setOption()
   */
  public function setOption($name, $value) {
    $this->_options[$name] = $value;
  }

  /**
   * (non-PHPdoc)
   * @see IOptionable::getOption()
   */
  public function getOption($name, $default = NULL) {
    if (isset($this->_options[$name])) {
      return $this->_options[$name];
    }
    return $default;
  }

  /**
   * (non-PHPdoc)
   * @see IOptionable::hasOption()
   */
  public function hasOption($name) {
    return isset($this->_options[$name]);
  }
}
