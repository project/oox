<?php

class OoxRegistryException extends OoxException { }

/**
 * Simple registry pattern implementation, using Drupal hook/cache systems
 * in order for module easily declare specific implementations.
 * 
 * In order to use it, the internal type must match the corresponding hook.
 * E.g. if you define the 'my_element' type, implementing modules must use
 * the hook_my_element() hook in order to expose their custom types.
 */
class OoxRegistry extends Registrable
{
  /**
   * Per type singleton implementation.
   * 
   * @var array
   *   Full with OoxRegistry instances, or overriding classes.
   */
  private static $__instances = array();

  /**
   * Ugly, but working simple registry of registries implementation.
   * 
   * @var array.
   */
  private static $__registries;

  /**
   * Get instance of given type.
   * 
   * @param string $type
   *   Registry type.
   * @param boolean $reset = FALSE
   *   If set to TRUE, it will call the hook_oox_registry() again in
   *   order to rebuild the internal registry cache, if modules have been
   *   loaded after the first call.
   *
   * @return OoxRegistry
   *   Specialized implementation.
   * 
   * @throws OoxRegistryException
   *   If registry type does not exists.
   */
  public static function getInstance($type, $reset = FALSE) {
    self::__loadCache($reset);
    // Lazzy load the registry when needed.
    if (!isset(self::$__instances[$type])) {
      $hook = NULL;
      $class = NULL;
      // Registries pseudo registry introspection. We are going to include the
      // right file on a on-demand basis.
      if (isset(self::$__registries[$type])) {
        // Find if there is a custom class given.
        if (isset(self::$__registries[$type]['class'])) {
          $class = self::$__registries[$type]['class'];
        }
        // Also check out for a given hook name.
        if (isset(self::$__registries[$type]['hook'])) {
          $hook = self::$__registries[$type]['hook'];
        }
      }
      // If not exists, then all we need is the classical object registry,
      // which does most of the job developers will need though.
      $registry = $class ? new $class($type) : new OoxRegistry($type);
      if ($hook) {
        $registry->_setHookName($hook);
      }
      self::$__instances[$type] = $registry;
    }
    return self::$__instances[$type];
  }

  /**
   * Load static registries cache.
   */
  private static function __loadCache($reset = FALSE) {
    if ($reset || !isset(self::$__registries)) {
      // FIXME: Handle at least some cache level here.
      self::$__registries = module_invoke_all('oox_registry');
    }
  }

  /**
   * Get all registries descriptions.
   * 
   * @return array
   */
  public static function _getAllDescriptions() {
    self::__loadCache();
    // FIXME: This should have not to be done, because we don't do caching
    // we do this filtering on the less asked method to keep the global static
    // initialization performant in every day's life.
    $ret = array();
    foreach (self::$__registries as $type => &$item) {
      if (!empty($item)) {
        $ret[$type] = $item;
      }
    }
    return $ret;
  }

  /**
   * Main constructor. This is protected due to the singleton pattern.
   * 
   * @param string $type
   *   Registry type.
   */
  protected function __construct($type) {
    $this->_setType($type);
  }

  /**
   * Internal cache.
   * 
   * @var array
   */
  protected $_cache;

  /**
   * Set to private to force overriding classes to use the _getHookName()
   * method instead.
   * 
   * @var string
   */
  private $__hookName;

  /**
   * Pragmatically change hook name for this registry. This will be usefull
   * for registry instanciation time, if module owner did not want to define
   * a new class only to change that hook name.
   * 
   * @param string $hook
   *   Hook name.
   */
  protected function _setHookName($hook) {
    $this->__hookName = $hook;
  }

  /**
   * Get hook name and cache identifier, if you don't like it, you can
   * override it.
   * 
   * @return string
   */
  public function _getHookName() {
    if (!isset($this->__hookName)) {
      $this->__hookName = 'oox_registry_' . $this->_type;
    }
    return $this->__hookName;
  }

  /**
   * Method you can use in extreme use cases, where you need all the class
   * files to be loaded. 
   */
  public function _requireAll() {
    $this->_loadItemCache();
    foreach ($this->_cache as &$item) {
      if (isset($item['file'])) {
        require_once $item['file'];
      }
    }
  }

  /**
   * (Re)populate internal cache.
   * 
   * Set to public with the internal proctected prefix _ so other modules can
   * access it in order to force cache rebuild.
   */
  public function _rebuildItemCache() {
    $this->_cache = array();
    $hook = $this->_getHookName();
    // Handle custom item ordering.
    $weights = variable_get($hook . '_weight', array());
    // Call hook on each module that intends to provide items for this custom
    // registry.
    foreach (module_implements($hook) as $module) {
      foreach (module_invoke($module, $hook) as $type => $item) {
        // Allow overriding class to add its own checks.
        if ($this->_checkItem($type, $item, $module)) {
          // If all ok, then add our item to cache.
          $this->_cache[$type] = $item;
          // Handle custom item ordering.
          if (isset($weights[$type])) {
            $this->_cache[$type]['weight'] = $weights[$type]; 
          }
          else {
            $this->_cache[$type]['weight'] = 0;
          }
        }
      }
    }
    // Real sorting, before saving the result.
    uasort($this->_cache, '_oox_registry_compare');
    cache_set($hook, $this->_cache, OOX_CACHE_TABLE);
  }

  /**
   * Check an item coming from a specific module hook implementation.
   * 
   * You are advised to override this method for specific implementations, don't
   * ever forget to call parent implementation.
   * 
   * @param string $type
   *   Element type name.
   * @param array &$item
   *   Raw item descriptive array from hook implementation.
   * @param string $module
   *   Original module name.
   * 
   * @return boolean
   *   TRUE if item is OK, false else.
   */
  protected function _checkItem($type, &$item, $module) {
    $item['module'] = $module;
    $item['type'] = $type;
    // Check for file, and compute its full absolute path at the same time.
    if (isset($item['file']) && (!$path = oox_get_full_path($item['file'], $module))) {
      unset($item['file']);
      watchdog('oox', "Module " . $module . " define the file " . $item['file'] . " which does not exists, type " . $type . " may fail being registered", NULL, WATCHDOG_ALERT);
    }
    // Trigger some warnings.
    if (isset($item['file'])) {
      watchdog('oox', "Key 'file' in registry description is deprecated, use hook_oox_api() instead. Error triggered by module '" . $module . "' for item '" . $type . "'.", NULL, WATCHDOG_ALERT);
    }
    // If file exists, load it for further verifications.
    if (isset($path)) {
      $item['file'] = $path;
      require_once $item['file'];
    }
    // Check for defined class.
    if (!isset($item['class']) || !class_exists($item['class'])) {
      watchdog('oox', "Module " . $module . " define the class '" . $item['class'] . "' which does not exists, unregistering object type " . $type, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Load cache from Drupal cache.
   */
  protected function _loadItemCache() {
    // Lazzy load the cache.
    if (!isset($this->_cache)) {
      $cid = $this->_getHookName();
      if ($cached = cache_get($cid, OOX_CACHE_TABLE)) {
        $this->_cache = $cached->data;
      }
      else {
        $this->_rebuildItemCache();
      }
    }
  }

  /**
   * Get back the internal cache array.
   * 
   * @return array
   *   Raw item descriptive arrays, given by modules with some modifications
   *   made by the _checkItem() method.
   */
  public function getItemCache() {
    $this->_loadItemCache();
    return $this->_cache;
  }

  /**
   * Get one instance for each type registered.
   * 
   * @return array
   *   Key/value pairs, keys are item type, and values are new object instances.
   */
  public function getAllItems() {
    $ret = array();
    $this->_loadItemCache();
    foreach ($this->_cache as $type => &$item) {
      $ret[$type] = $this->getItem($type);
    }
    return $ret;
  }

  /**
   * Get new element instance. You can override it, but be aware of cache load
   * if you do so.
   * 
   * @param string $type
   *   Element type.
   * 
   * @return mixed
   *   Type instance.
   * 
   * @throws OoxRegistryException
   *   If type is unknown, or description is incomplete (such as at install or
   *   early bootstrap stagees, the autoload function may or may not have been
   *   registered yet by the oox module).
   */
  public function getItem($type) {
    $this->_loadItemCache();

    // Check type exists.
    if (!isset($this->_cache[$type])) {
      throw new OoxRegistryException("Element type " . $type . " is unknown for registry type " . $this->_type);
    }

    // Create new instance and return element if all ok.
    if (isset($this->_cache[$type]['file'])) {
      require_once $this->_cache[$type]['file'];
    }

    $class = $this->_cache[$type]['class'];

    // This may happen, when installing or updating.
    if (!class_exists($class)) {
      print_r(debug_backtrace());die();
      throw new OoxRegistryException("Early bootstrap, autoload function seems not be registered yet");
    }

    // Create new item.
    $item = new $class();

    if ($item instanceof IRegistrable) {
      $item->_setType($type);
      $item->_setRegistryType($this->_type);
    }

    return $item;
  }
}

/**
 * Small static helper for registry items sorting.
 */
function _oox_registry_compare($a, $b) {
  if ($a['weight'] == $b['weight']) {
    return strcmp($a['type'], $b['type']);
  }
  else {
    return $a['weight'] - $b['weight'];
  }
}
