<?php
// $Id

/**
 * Base exception for this module, every exception thrown by any piece of this
 * API should be a specialization of this one.
 */
class OoxException extends Exception { }
