<?php

/**
 * Define an object that can be used in registries.
 * 
 * Any object can be used in registries, this particular ones can have a type
 * property, usefull for dynamic processing using machine names instead of
 * testing instances classes.
 */
interface IRegistrable
{
  /**
   * Public, but internal method, for registries. At instanciation time, the
   * registry will set the internal type using this method.
   * 
   * @param string $type
   *   Registry machine name.
   */
  public function _setRegistryType($type);

  /**
   * Get the internal registry type (defined as key in the oox registry
   * hook).
   * 
   * @return string
   *   Registry machine name.
   */
  public function getRegistryType();

  /**
   * Public, but internal method, for registries. At instanciation time, the
   * registry will set the internal type using this method.
   * 
   * @param string $type
   *   Class machine name.
   */
  public function _setType($type);

  /**
   * Get the internal object type (defined as key in the registry hook).
   * 
   * @return string
   *   Class machine name.
   */
  public function getType();
}

/**
 * Default implementation that can serve as objects base for implementing
 * the IRegistrable interface.
 */
abstract class Registrable implements IRegistrable
{
  /**
   * @var string
   */
  protected $_registryType;

  /**
   * (non-PHPdoc)
   * @see IRegistrable::_setRegistryType()
   */
  public function _setRegistryType($type) {
    $this->_registryType = $type;
  }

  /**
   * (non-PHPdoc)
   * @see IRegistrable::getRegistryType()
   */
  public function getRegistryType() {
    return $this->_registryType;
  }

  /**
   * @var string
   */
  protected $_type;

  /**
   * (non-PHPdoc)
   * @see IRegistrable::_setType()
   */
  public final function _setType($type) {
    $this->_type = $type;
  }

  /**
   * (non-PHPdoc)
   * @see IRegistrable::getType()
   */
  public final function getType() {
    return $this->_type;
  }
} 
