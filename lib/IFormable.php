<?php

/**
 * Specific exception thrown when a IFormable object does not validate its form.
 */
class OoxFormValidationException extends OoxException
{
  protected $_errors = array();

  /**
   * Default constructor.
   * 
   * @param array $errors = array()
   *   (optional) Array of errors, keys are Drupal FAPI element pathes, values
   *   are localized error messages.
   */
  public function __construct($errors = array()) {
    parent::__construct("Errors happend during form validation");
    if (!empty($errors)) {
      $this->_errors = $errors;
    }
  }

  /**
   * Add error to this exception.
   * 
   * @param $path
   *   Drupal FAPI element path.
   * @param $message
   *   Localized error messages.
   */
  public function addError($path, $message) {
    $this->_errors[$path] = $message;
  }

  /**
   * Get all errors.
   * 
   * @return array
   *   Array of errors, keys are Drupal FAPI element pathes, values are
   *   localized error messages.
   */
  public function getErrors() {
    return $this->_errors;
  }
}

/**
 * This interface represents any kind of objects that can be configured using
 * a form. Notice this is a really global 'configuration' concept.
 * 
 * If you need to do any other form that a form used to configure the object
 * itself, then consider building your own Drupal form in you custom module
 * instead of trying to use this.
 * 
 * This will mainly be used by common administration pages code, in order to
 * include objects implementation specific form additions for objects editing.
 */
interface IFormable
{
  /**
   * Build the form.
   * 
   * The builded form may be a global form subset, with #tree property always
   * set, so please care about this.
   * 
   * @return array
   *   Drupal FAPI elements set.
   */
  public function form();

  /**
   * Validate the form.
   * 
   * Values are the value subset corresponding to the form() method returned
   * elements.
   * 
   * @param array &$values
   *   Form values, can be an empty array if form() method did not returned
   *   any elements.
   *
   * @throws OoxFormValidationException
   *   If form does not validate.
   */
  public function formValidate(&$values);

  /**
   * Submit the form, do whatever you have to do, it can be database updates
   * or the current object properties update.
   * 
   * Values are the value subset corresponding to the form() method returned
   * elements.
   * 
   * @param array &$values
   *   Form values, can be an empty array if form() method did not returned
   *   any elements.
   */
  public function formSubmit(&$values);
}
